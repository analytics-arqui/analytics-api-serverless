import psycopg2
import os
import json


HOST = os.environ['HOST']
USERNAME = os.environ['USERNAME']
PASSWORD = os.environ['PASSWORD']

conn = psycopg2.connect(database="postgres",
                        host=HOST,
                        user=USERNAME,
                        password=PASSWORD,
                        port="5432")
cursor = conn.cursor()

def getLastFive(event, context):
    
    cursor.execute("select * from temp limit 5")
    
    return {
        'statusCode': 200,
        'body': json.dumps(cursor.fetchmany(size=5))
    }

def getAllDateCounts(event, context):
    cursor.execute('SELECT * FROM public."agrupFecha"')
    return {
        'statusCode': 200,
        'body': json.dumps(cursor.fetchall())
    }

def getHoursByDate(event, context):
    body = json.loads(event['body'])
    fecha = body.get("date")
    cursor.execute(f"select * from agrup where fecha = '{fecha}'")
    return {
        'statusCode': 200,
        'body': json.dumps(cursor.fetchall())
    }            